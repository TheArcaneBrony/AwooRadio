using AwooRadio.Shared;

namespace AwooRadio.API
{
    public class TestData
    {
        public static void Load()
        {
            Program.Config = new Config()
            {
                Stations =
                {
                    new RadioStation(){ Name = "Nostalgie", Urls = {"https://21293.live.streamtheworld.com/NOSTALGIEWHATAFEELING.mp3", "https://playerservices.streamtheworld.com/api/livestream-redirect/NOSTALGIEWHATAFEELINGAAC.aac", "https://22193.live.streamtheworld.com/NOSTALGIEWHATAFEELING.mp3"}}
                }
            };
        }
    }
}