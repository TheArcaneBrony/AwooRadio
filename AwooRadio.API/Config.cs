using System.Collections.Generic;
using AwooRadio.Shared;

namespace AwooRadio.API
{
    public class Config
    {
        public List<RadioStation> Stations { get; set; }= new();
    }
}