﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace AwooRadio.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ChannelController : ControllerBase
    {

        [HttpGet("/channels")]
        public string GetChannels()
        {
            return JsonConvert.SerializeObject(Program.Config.Stations, Formatting.Indented);
        }
    }
}