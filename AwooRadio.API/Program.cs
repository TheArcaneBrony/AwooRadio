using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace AwooRadio.API
{
    public class Program
    {
        public static Config Config = new Config();
        public static void Main(string[] args)
        {
            if (File.Exists("config.json")) Config = JsonConvert.DeserializeObject<Config>(File.ReadAllText("config.json"));
            else TestData.Load();
            File.WriteAllText("config.json", JsonConvert.SerializeObject(Config, Formatting.Indented));
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });
    }
}