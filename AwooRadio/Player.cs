using System.Collections.Generic;
using Android.Media;

namespace AwooRadio
{
    public class Player
    {
        public static MediaPlayer player = new();
        private static bool IsPrepared = false;
        private static int urlindex = 0;
        public static bool Play(List<string> url, int? index = null)
        {
            urlindex = index ?? 0;
            if (!IsPrepared)
            {
                if (player == null)
                    player = new MediaPlayer();
                else
                    player.Reset();

                player.SetDataSource(url[urlindex++%url.Count]);
                player.PrepareAsync();
            }

            player.Prepared += (sender, args) =>
            {
                player.Start();
                IsPrepared = true;
            };
            player.Error += (sender, args) => Play(url); 
            return true;
        }
        
        public static void Pause()
        {
            player.Pause();
        }

        public static void Stop()
        {
            player.Stop();
            IsPrepared = false;
        }
    }
}