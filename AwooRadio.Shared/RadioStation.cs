﻿using System;
using System.Collections.Generic;

namespace AwooRadio.Shared
{
    public class RadioStation
    {
        public string Name = "";
        public List<string> Urls = new();
    }
}